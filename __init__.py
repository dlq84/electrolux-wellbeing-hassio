"""Electrolux wellbeing integration."""

import json
import logging
from .const import DOMAIN

from homeassistant.components.vacuum import DOMAIN as VACUUM_DOMAIN

from homeassistant.core import HomeAssistant
from homeassistant.config_entries import ConfigEntry

_LOGGER = logging.getLogger(__name__)

async def async_setup_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:

  _LOGGER.info(entry.data)
  hass.data.setdefault(DOMAIN, {})

  hass.async_create_task(
    hass.config_entries.async_forward_entry_setup(entry, VACUUM_DOMAIN)
  )
  return True
