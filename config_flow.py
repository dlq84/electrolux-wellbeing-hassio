from pprint import pprint
from homeassistant.config_entries import CONN_CLASS_CLOUD_POLL, ConfigEntry, ConfigFlow
from homeassistant.const import CONF_EMAIL, CONF_PASSWORD, CONF_CLIENT_SECRET, CONF_TOKEN
from .const import DOMAIN
import voluptuous as vol
import logging
from electrolux_wellbeing import WellbeingApi

_LOGGER = logging.getLogger(__name__)

class ElectroluxWellbeingConfigFlow(ConfigFlow, domain=DOMAIN):
  VERSION = 1
  CONNECTION_CLASS = CONN_CLASS_CLOUD_POLL

  async def async_step_user(self, user_input: dict = None) -> dict:
    _LOGGER.debug("async_step_user")
    _LOGGER.debug(user_input)
    if user_input is not None:
      api = WellbeingApi(user_input[CONF_CLIENT_SECRET])
      await self.hass.async_add_executor_job(api.init_client_token)
      # self.refresh_token = api.login(user_input[CONF_EMAIL], user_input[CONF_PASSWORD])
      self.refresh_token = await self.hass.async_add_executor_job(api.login, user_input[CONF_EMAIL], user_input[CONF_PASSWORD])

      _LOGGER.debug(self.refresh_token)

      self.client_secret = user_input[CONF_CLIENT_SECRET]
      return await self.async_step_installation()

    return self.async_show_form(
      step_id="user", data_schema=vol.Schema({
        vol.Required(CONF_CLIENT_SECRET): str,
        vol.Required(CONF_EMAIL): str,
        vol.Required(CONF_PASSWORD): str
      })
    )

  async def async_step_installation(self, user_input: dict = None) -> dict:
    """Select Verisure installation to add."""
    return self.async_create_entry(
      title="Electrolux Wellbeing",
      data={
        CONF_TOKEN: self.refresh_token,
        CONF_CLIENT_SECRET: self.client_secret
      },
    )
