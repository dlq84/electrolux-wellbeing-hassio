
"""Platform for electolux wellbeing vacuum."""
import logging
import time
from typing import Any, Callable, Iterable

import voluptuous as vol

from homeassistant.core import HomeAssistant, callback
from homeassistant.config_entries import ConfigEntry
from homeassistant.helpers.entity import Entity
import homeassistant.helpers.config_validation as cv
from homeassistant.components.vacuum import (
  VacuumEntity,
  PLATFORM_SCHEMA,
  STATE_CLEANING,
  STATE_DOCKED,
  STATE_PAUSED,
  STATE_RETURNING,
  STATE_ERROR,
  SUPPORT_START,
  SUPPORT_PAUSE,
  SUPPORT_RETURN_HOME,
  SUPPORT_FAN_SPEED,
)

from homeassistant.const import CONF_TOKEN, CONF_CLIENT_SECRET
from .const import DOMAIN

from electrolux_wellbeing import WellbeingApi
from electrolux_wellbeing.models import Appliance

_LOGGER = logging.getLogger(__name__)

FAN_SPEED = {
  "Quiet": 1,
  "Smart": 2,
  "Power": 3,
}

async def async_setup_entry(
    hass: HomeAssistant,
    entry: ConfigEntry,
    async_add_entities: Callable[[Iterable[Entity]], None],
) -> None:
  _LOGGER.debug(entry)

  api = WellbeingApi(entry.data[CONF_CLIENT_SECRET], entry.data[CONF_TOKEN])
  await hass.async_add_executor_job(api.refresh_token)
  appliances = await hass.async_add_executor_job(api.get_appliances)
  async_add_entities([HassioVacuum(api, x) for x in appliances if x.model_name == "PUREi9"])

class HassioVacuum(VacuumEntity):
  _appliance: dict = None

  def __init__(self, api: WellbeingApi, vacuum: Appliance):
    _LOGGER.debug("__init__")
    self._api = api
    self._vacuum = vacuum
    self._name = vacuum.appliance_name
    self._state = None
    # self.schedule_update_ha_state(force_refresh=True)
    # self._appliance = await ss.async_add_executor_job(api.get_appliance, vacuum)

  @property
  def supported_features(self):
    return SUPPORT_START | SUPPORT_PAUSE | SUPPORT_RETURN_HOME | SUPPORT_FAN_SPEED

  @property
  def should_poll(self):
    return True

  @property
  def name(self):
    return self._name

  @property
  def device_info(self):
    return {
      "identifiers": {
          # Serial numbers are unique identifiers within a specific domain
          (DOMAIN, self._vacuum.pnc_id)
      },
      "name": self._name,
      "manufacturer": "Electrolux",
      "model": self._vacuum.model_name,
      "sw_version": "0.0.0"
      # "via_device": (DOMAIN, self.api.bridgeid),
    }

  @property
  def state(self):
    if self._appliance is None:
      return None
    status = self._appliance["twin"]["properties"]["reported"]["robotStatus"]
    if status == 1:
      return STATE_CLEANING
    elif status == 2 or status == 6:
      return STATE_PAUSED
    elif status == 5:
      return STATE_RETURNING
    elif status == 10:
      return STATE_DOCKED
    else:
      return STATE_ERROR

  @property
  def error(self):
    return "ERROR"

  @property
  def fan_speed_list(self):
    return ["Quiet", "Smart", "Power"]

  @property
  def fan_speed(self):
    if self._appliance is None:
      return "none"
    powerMode = self._appliance["twin"]["properties"]["reported"]["powerMode"]
    if powerMode == 1:
      return "Quiet"
    if powerMode == 2:
      return "Smart"
    if powerMode == 3:
      return "Power"
    return "none"

  def set_fan_speed(self, fan_speed: str):
    self._api.change_settings(self._vacuum, {"PowerMode": FAN_SPEED[fan_speed]})
    time.sleep(2)

  def start(self):
    self._api.send_command(self._vacuum, {"CleaningCommand": "Play"})

  async def async_start(self):
    await self.hass.async_add_executor_job(self.start)

  def pause(self):
    self._api.send_command(self._vacuum, {"CleaningCommand": "Pause"})

  async def async_pause(self):
    await self.hass.async_add_executor_job(self.pause)

  def return_to_base(self):
    self._api.send_command(self._vacuum, {"CleaningCommand": "Home"})

  def update(self):
    self._appliance = self._api.get_appliance(self._vacuum)
